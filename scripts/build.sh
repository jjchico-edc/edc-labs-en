#!/bin/sh -e

paths="atari_rom gate_control simple_calculator simple_rpn_calculator simple_computer_1 simple_computer_2 avr_intro avr_dice"
mkdir -p build

for dir in $paths; do
    cd "$dir"
    if ls *.odt; then
        lowriter --convert-to pdf --outdir ../build *.odt
    fi
    if [ -d *lab_kit ]; then
        rm -f "../build/${dir}_lab_kit.zip"
        zip -r "../build/${dir}_lab_kit.zip" *lab_kit
    fi
    cd ..
done
