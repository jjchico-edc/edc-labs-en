// Module: system
// Description: Barrier control with timer
// Project: Barrier control system
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Initial date: 21-02-2019

module system #(
    parameter SYS_FREQ = 50000000   // system clock frequency (Hz)
    )(
    input wire clk,         // system clock
    input wire reset,       // system initialization
    input wire open,        // opening signal (active high)
    input wire obs,         // obstacle detector (active low)
    output wire gate,       // control output (0-close, 1-open)
    output wire servo       // servo control output
    );

    wire eoc;       // timer's end of count
    wire clear;     // timer's clear
    wire enable;    // timer's enable
    reg obs_reg;    // synchronized obs input
    reg open_reg;   // synchronized open input

    // Registered (synchronized) obs and open inputs
    always @(posedge clk) begin
        obs_reg <= obs;
        open_reg <= open;
    end
    
    // servo_gate module instance
    servo_gate #(
        .FS(SYS_FREQ)
        ) servo_gate (
        .clk(clk),
        .gate(gate),
        .servo(servo),
        .finished(finished)
        );

    /*** instantiate and connect three modules: control, timer, servo_gate ***/

endmodule // system
