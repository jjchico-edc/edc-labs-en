// Module: control
// Description: Barrier control with timer. Unidad de control.
// Project: Barrier control system
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Initial date: 21-02-2019

module control (
    input wire clk,         // system clock
    input wire reset,       // system initialization
    input wire open,        // opening signal (active high)
    input wire eoc,         // timer's end of count
    input wire obs,         // obstacle detector (active low)
    input wire finished,    // gate finished moving
    output reg clear,       // timer's clear
    output reg enable,      // timer's enable
    output reg gate         // control output (0-close, 1-open)
    );

    // states
    localparam CLOSED  = 2'd0,
               OPEN    = 2'd1;

               /*** add more states if necessary ***/

    // state and next state variables
    reg [1:0] state = CLOSED;   // initial state = CLOSED
    reg [1:0] next_state;

    // state change process

    /*** add your code ***/

    // next state and output process

    /*** add your code ***/

endmodule // control
