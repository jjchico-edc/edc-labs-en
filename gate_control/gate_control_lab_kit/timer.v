// Module: timer
// Description: Timer
// Project: Barrier control system
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Initial date: 21-02-2019

// Counter/timer

/*
   We need a counter that can be enabled and disabled, set to zero (cleared)
   and with an end-of-count output signal that activates when an appropriate
   maximum value is reached.
 */

module timer #(
    parameter SYS_FREQ = 50000000,  // system clock rrequency (Hz)
    parameter DELAY = 10            // time to end-of-count (s)
    )(
    input wire clk,         // system clock
    input wire clear,       // system initialization
    input wire enable,      // enable
    output wire eoc         // end of count
    );

    // conter's maximum value (end-of-count is generated when reached)
    localparam MAX_COUNT = SYS_FREQ * DELAY - 1;

    // counter state and internal signals
    /* must be wide enough to hold a value up to MAX_COUNT */
    reg [/*** ***/] count = 0;      // state

    // counter's description

    /*** counter's code ***/

    /*** end-of-count code ***/

endmodule // timer
