// Module: system - banco de pruebas
// Description: Banco de pruebas del sistema
// Project: Barrier control system
// Autor: Jorge Juan-Chico <jjchico@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Initial date: 21-02-2019

`timescale 1ns / 1ps

module test();

    localparam SYS_FREQ = 100;            // system frequency (Hz)
    localparam CYCLE = 10**9/SYS_FREQ;    // period (ns)

    reg clk;        // system clock
    reg reset;      // system initialization
    reg open;       // opening signal (active high)
    reg obs;        // obstacle detector (active low)
    wire gate;      // control output (0-close, 1-open)
    wire servo;     // servo control output

    // Design under test
    system #(
        .SYS_FREQ(SYS_FREQ)
        ) dut (
        .clk(clk),
        .reset(reset),
        .obs(obs),
        .open(open),
        .servo(servo),
        .gate(gate)
        );

    // Clock generator
    always
        #(CYCLE/2) clk = ~clk;

    initial begin
        clk = 0;
        reset = 0;
        open = 0;
        obs = 1;

        // waveform file
        $dumpfile("system_tb.vcd");
        // store all waveforms in "test"
        $dumpvars(0, test);

        // main simulation control
        repeat(1*SYS_FREQ) @(negedge clk);      // wait 1s
        reset = 1;                              // reset
        repeat(1*SYS_FREQ) @(negedge clk);
        reset = 0;
        repeat(1*SYS_FREQ) @(negedge clk);      // wait 1s
        open = 1;                               // button press
        repeat(1*SYS_FREQ) @(negedge clk);
        open = 0;
        repeat(5*SYS_FREQ) @(negedge clk);      // wait 5s
        if(gate != 1'b1)
            $display("ERROR: %t: gate=%b, expected: 1.", $time, gate);
        repeat(10*SYS_FREQ) @(negedge clk);     // wait 10s
        if(gate != 1'b0)
            $display("ERROR: %t: gate=%b, expected: 0.", $time, gate);
        repeat(1*SYS_FREQ) @(negedge clk);      // wait 1s
        open = 1;                               // button press
        repeat(1*SYS_FREQ) @(negedge clk);
        open = 0;
        repeat(5*SYS_FREQ) @(negedge clk);      // wait 5s
        open = 1;                               // button press
        repeat(1*SYS_FREQ) @(negedge clk);
        open = 0;
        repeat(8*SYS_FREQ) @(negedge clk);      // wait 8s
        obs = 0;                                // obstacle detected
        repeat(4*SYS_FREQ) @(negedge clk);      // wait 4s
        if(gate != 1'b1)
            $display("ERROR: %t: gate=%b, expected: 1.", $time, gate);
        obs = 1;                                // obstacle removed
        repeat(2*SYS_FREQ) @(negedge clk);      // wait 2s
        obs = 0;                                // obstacle detected
        repeat(3*SYS_FREQ) @(negedge clk);      // wait 3s
        obs = 1;                                // obstacle removed
        repeat(10*SYS_FREQ) @(negedge clk);     // wait 10s
        if(gate != 1'b0)
            $display("ERROR: %t: gate=%b, expected: 0.", $time, gate);

        $finish;
    end
endmodule
